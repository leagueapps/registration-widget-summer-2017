import React, { Component } from 'react'

import Select from 'react-select'
import 'react-select/dist/react-select.css'

export default class MultiSelect extends Component {
	
	constructor(props) {
		super(props)
		this.state = {
			currentOptions : null,
			options : this.props.options
		}
		this.onChange = this.onChange.bind(this)
	}
	
	onChange(e) {
		this.setState({ currentOptions : e })
		this.props.onChange(e)
	}
	
	render() {
		return (
			<Select multi joinValues
				placeholder={ this.props.placeholder }
				value={ this.state.currentOptions }
				options={ this.props.options }
				onChange={ this.onChange }
			/>
		)
	}
	
	componentWillReceiveProps(props) {
		this.setState({
			options : props.options
		})
	}
	
}