import { capitalizeFirstLetters } from './util'



const sortArrayOfObjects = (arrayOfObjects, propertyToSortBy) => {
	return arrayOfObjects.sort((a, b) => {
		if(a[propertyToSortBy] < b[propertyToSortBy]) return -1
		if(a[propertyToSortBy] > b[propertyToSortBy]) return 1
		return 0
	})
}

export { sortArrayOfObjects }



const daysMappedToI = {
	'Mon' : 0,
	'Tue' : 1,
	'Wed' : 2,
	'Thu' : 3,
	'Fri' : 4,
	'Sat' : 5,
	'Sun' : 6
}

const iMappedToDays = [ 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun' ]

export const addOptions = (self) => {

	Array.from([
		{ inState : 'daysOptions', inListings : 'scheduleDays' },
		{ inState : 'programTypesOptions', inListings : 'type' },
		{ inState : 'sportOptions', inListings : 'sport' },
		{ inState : 'seasonOptions', inListings : 'season' },
		{ inState : 'levelOptions', inListings : 'experienceLevel' },
		{ inState : 'locationOptions', inListings : 'location' },
		{ inState : 'genderOptions', inListings : 'gender' }
	]).forEach((o) => {
	
		let uniqueOptions = []
		self.state.listings.forEach((listing) => {
			if( uniqueOptions.indexOf( listing[o.inListings] ) === -1 )
				uniqueOptions.push( listing[o.inListings] )
		})
		
		let optionsFormatted = []
		uniqueOptions.forEach((e, i) => {
			if(e !== undefined && e !== '') {
			
				let label = e
				
				if( o.inListings === 'scheduleDays' ) {
				
					label = label.split(',').map((day) => {
						return daysMappedToI[day]
					}).sort().join(',')
					
				}

				if(label === 'CLUBTEAM')
					label = 'Club Team'
				if(label === 'CO_ED')
					label = 'Coed'
				if(label === 'ANY')
					return
				
				label = capitalizeFirstLetters(label)
				
				optionsFormatted.push({
					'label' : label,
					'value' : e
				})
				
			}
		})
		
		switch(o.inListings) {
		
			case 'scheduleDays' : {
				sortArrayOfObjects(optionsFormatted, 'label')
				optionsFormatted.forEach((formattedOption) => {
					formattedOption.label = formattedOption.label.split(',').map((iOfDay) => {
						return iMappedToDays[iOfDay]
					}).join(', ')
				})
				break
			}
		
			case 'type' :
			case 'sport' :
			case 'season' :
			case 'experienceLevel' :
			case 'location' :
				sortArrayOfObjects(optionsFormatted, 'label')
				break
			
			default : {
				break
			}
			
		}
		
		self.setState({
			[o.inState] : [ ...self.state[o.inState], ...optionsFormatted ]
		})
		
	})
	
	Array.from([
		{ label : 'Activity End Date', value : 'endTime' },
		{ label : 'Registration Start Date', value : 'regularRegistrationTime' },
		{ label : 'Registration End Date', value : 'registrationEndTime' },
		{ label : 'Alphabetical (A-Z)', value : 'name' }
	]).forEach((option) => {
	
		if((() => {
		
			let returnValue = true
			
			self.state.listings.forEach((listing) => {
				if(listing[option.value] === undefined)
					returnValue = false
			})
			
			return returnValue
			
		})()) {
			self.state.sortByOptions.push(option)
		}
		
	})

}



export const shouldShowListing = (self, listing) => {
	return (
					
		(
			(self.state.days.length === 0) ||
			(() => {
				let returnValue = false
			
				self.state.days.forEach((d) => {
					if( listing.scheduleDays === d ) {
						returnValue = true
					}
				})
			
				return returnValue
			})()
		) &&
		
		(
			(self.state.programTypes.length === 0) ||
			(() => {
				let returnValue = false
			
				self.state.programTypes.forEach((type) => {
					if( listing.type === type )
						returnValue = true
				})
			
				return returnValue
			})()
		)&&
	
		( self.state.sport === 'All Sports' ||
		self.state.sport === listing.sport) &&
		
		( self.state.season === 'All Seasons' ||
		self.state.season === listing.season ) &&
		
		( self.state.level === 'All Levels' ||
		self.state.level === listing.experienceLevel ) &&
		
		( self.state.location === 'All Locations' ||
		self.state.location === listing.location ) &&
		
		(
			self.state.gender === 'All Genders' ||
			(
				self.state.gender === listing.gender ||
				( listing.gender === 'CO_ED' || listing.gender === 'ANY' ) && self.state.gender === 'Coed'
			)
		)
	)
}



export const sortListings = (self) => {
	self.setState({
		listings : sortArrayOfObjects(self.state.listings, self.state.sortBy)
	})
}