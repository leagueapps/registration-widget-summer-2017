
export const capitalizeFirstLetters = (s) => {
	let lowerCasedWords = s.toLowerCase().split(' ')
	let returnS = lowerCasedWords[0].charAt(0).toUpperCase() + lowerCasedWords[0].slice(1)
	if(lowerCasedWords.length > 1)
		returnS += ' '
	
	for(var i = 1; i < lowerCasedWords.length; i++) {
		returnS +=  lowerCasedWords[i].charAt(0).toUpperCase() + lowerCasedWords[i].slice(1)
		if (i < lowerCasedWords.length - 1)
			returnS += ' '
	}
	
	return returnS
}

const months = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ]

export const monthDayYear = (timestamp) => {
	let date = new Date(timestamp)
	return months[date.getMonth()] + ' ' + date.getDate() + " '" + String(date.getYear()).substr(-2)
}

export const formatPrice = (price) => {
	return String(price).split('.')[0] + '.00'
}