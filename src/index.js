import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import fetch from 'fetch-jsonp'
import WebFont from 'webfontloader'

import SingleSelect from './components/single-select'
import MultiSelect from './components/multi-select'
import Listing from './components/listing'

import {
	addOptions,
	shouldShowListing,
	sortListings
} from './filter-and-sort'

import './style.css'

class RegistrationWidget extends Component {
  
  constructor(props) {
  	super(props)
  	
  	// load a webfont (accessible)
  	WebFont.load({ google : { families : [ 'Raleway', 'Sans Serif'] } })
  	
  	// initial state
  	this.state = {
  	
  		theme : [],
  		
  		listings : [],
  		
  		// all options
		daysOptions : [],
		programTypesOptions : [],
		sportOptions : [ { label : 'All Sports', value : 'All Sports' } ],
		seasonOptions : [ { label : 'All Seasons', value : 'All Seasons' } ],
		levelOptions : [ { label : 'All Levels', value : 'All Levels' } ],
		locationOptions : [ { label : 'All Locations', value : 'All Locations' } ],
		genderOptions : [ { label : 'All Genders', value : 'All Genders' } ],
		sortByOptions : [ { label : 'Activity Start Date', value : 'startTime' } ],
  		
  		
  		// currently selected
		days : [],
		programTypes : [],
		sport : 'All Sports',
		season : 'All Seasons',
		level : 'All Levels',
		location : 'All Locations',
		gender : 'All Genders',
  		sortBy : 'startTime'
  		
  	}
  	
  	// prep the request url & parameters
  	const listingsRequest = `https://api.leagueapps.com/v1/sites/${ this.props.siteId }/programs/current?x-api-key=${ this.props.apiKey }`
  	
  	// call the JsonP request method (returns a promise)
  	fetch(listingsRequest).then((response) => {
		return response.json()
	}).then((data) => {
		
		// add masterName to each entry
		data.forEach((la) => {
			data.forEach((lb) => {
				if(la.masterProgramId === lb.programId)
					la.masterName = lb.name
			})
		})
		
		// get rid of session based masters
		data.forEach((listing, i) => {
			if(listing.isMaster && listing.isSessionBased) {
				delete data[i]
			}
		})
		
		// log it out for debugging
		console.log(data)
	
		// save the data
		this.setState({ listings : data }, () => {
			
			// add available options to the filters
			addOptions(this)
			
			// and sort the listing data (will trigger a re-rendering of listing components in the corresponding order)
			sortListings(this)
			
		})
		
	}).catch((error) => {
		console.log('error', error)
	})
	
	// get theme data
	const themeRequest = `https://api.leagueapps.com/v1/sites/${ this.props.siteId }?x-api-key=${ this.props.apiKey }`
	fetch(themeRequest).then((response) => {
		return response.json()
	}).then((data) => {
		this.setState({ theme : data.theme.colors }, () => console.log(this.state.theme))
	}).catch((error) => {
		console.log('error', error)
	})
	
  }
  
  render() {
    return (
    	<div className='league-apps-registration-widget'>
    		<div className='controls'>
    			
    			<MultiSelect
    				placeholder='All Days'
    				options={ this.state.daysOptions }
    				onChange={ (e) => {
    					this.setState({
    						days : e.map((d) => {
    							return d.value
    						})
    					})
    				} }
    			/>
    			
    			<MultiSelect
    				placeholder='All Program Types'
    				options={ this.state.programTypesOptions }
    				onChange={ (e) => {
    					this.setState({
    						programTypes : e.map((p) => {
    							return p.value
    						})
    					})
    				} }
    			/>
    			
    			<SingleSelect
    				options={ this.state.sportOptions }
    				onChange={ (e) => {
    					this.setState({
    						sport : e.value
    					})
    				} }
    			/>
    			
    			<SingleSelect
    				options={ this.state.seasonOptions }
    				onChange={ (e) => {
    					this.setState({
    						season : e.value
    					})
    				} }
    			/>
    			
    			<SingleSelect
    				options={ this.state.levelOptions }
    				onChange={ (e) => {
    					this.setState({
    						level : e.value
    					})
    				} }
    			/>
    			
    			<SingleSelect
    				options={ this.state.locationOptions }
    				onChange={ (e) => {
    					this.setState({
    						location : e.value
    					})
    				} }
    			/>
    			
    			<SingleSelect
    				options={ this.state.genderOptions }
    				onChange={ (e) => {
    					this.setState({
    						gender : e.value
    					})
    				} }
    			/>
    			
    			<SingleSelect
    				options={ this.state.sortByOptions }
    				onChange={ (e) => {
    					this.setState({
    						sortBy : e.value
    					}, () => {
    						sortListings(this)
    					} )
    				} }
    			/>
    			
    		</div>
    		<div className='listings' style={ this.state.theme.widgetBackground ? { color : this.state.theme.widgetBackground } : { color : '#000000' } }>
    		
				{ this.state.listings.map((listing, i) => {
					if(shouldShowListing(this, listing)) {
						return <Listing
							key={ i }
							data={ listing }
							theme={ this.state.theme }
						/>
					}
					return false
				}) }
				
    		</div>
    	</div>
    )
  }
  
}

((roots) => {
	roots.forEach((root) => {
		ReactDOM.render(
			<RegistrationWidget
				apiKey={ root.dataset.apiKey }
				siteId={ root.dataset.siteId } />,
			root
		)
	})
})(document.querySelectorAll('[data-league-apps-registration-widget-root]'))
