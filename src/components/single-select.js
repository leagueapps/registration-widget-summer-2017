import React, { Component } from 'react'

import Select from 'react-select'
import 'react-select/dist/react-select.css'

export default class SingleSelect extends Component {
	
	constructor(props) {
		super(props)
		this.state = {
			currentOption : this.props.options[0],
			options :  this.props.options
		}
		this.onChange = this.onChange.bind(this)
	}
	
	onChange(e) {
		this.setState({ currentOption : e })
		this.props.onChange(e)
	}
	
	render() {
		return (
			<Select joinValues
				value={ this.state.currentOption }
				options={ this.state.options }
				onChange={ this.onChange }
			/>
		)
	}
	
	componentWillReceiveProps(props) {
		this.setState({ options : props.options })
	}
	
}