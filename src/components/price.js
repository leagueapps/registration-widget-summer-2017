import React, { Component } from 'react'

import { formatPrice } from '../util'

export default class Price extends Component {
	
	render() {
		if(this.props.cost)
			return (
				<div className='a-is-b'>
					<div>{ this.props.name }:&nbsp;</div>
					<div>${ formatPrice(this.props.cost) }{(() => {
						if(this.props.percentProcessingFee)
							return ' + ' + this.props.percentProcessingFee + '% processing fee'
						else if(this.props.dollarProcessingFee)
							return ' + $' + formatPrice(this.props.dollarProcessingFee) + ' processing fee'
					})()}</div>
				</div>
			)
		else
			return <span></span>
	}
	
}