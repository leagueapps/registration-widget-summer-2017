import React, { Component } from 'react'

import { capitalizeFirstLetters, monthDayYear } from '../util'
import Price from './price'

export default class Listing extends Component {
	
	render() {
		return (
			<div className='listing'>
				<a href={ (() => {
					if(this.props.data.isMaster && this.props.data.isSessionBased) {
						return this.props.data.programUrlHtml
					} else {
						return this.props.data.registerUrlHtml
					}
				})() } target='_blank' rel='noopener noreferrer'>
			
					<div className='heading'>
						{ this.props.data.name &&
							<a href={ this.props.data.programUrlHtml } target='_blank' rel='noopener noreferrer'>
								<div className='title' style={ this.props.theme.widgetButton ? { color : this.props.theme.widgetButton } : { color : '#000000' } }>{ this.props.data.name }</div>
							</a>
						}
						{ this.props.data.masterName &&
							<div className='master' style={ this.props.theme.headings ? { color : this.props.theme.headings } : { color : '#000000' } }>{ this.props.data.masterName }</div>
						}
					</div>
				
					<div className='details'>
						
						<div className='listing-section times-and-location'>
						
							<div className='listing-section-heading'>Times &#38; Place:</div>
					
							{ this.props.data.startTime &&
								<div className='a-is-b'>
									<div>Activity dates:&nbsp;</div>
									<div>
										{ (() => {
											let activityTimes = monthDayYear(this.props.data.startTime)
											if(this.props.data.endTime)
												activityTimes += `-${ monthDayYear(this.props.data.endTime) }`
											return activityTimes
										})()}
									</div>
								</div>
							}
					
							{ this.props.data.regularRegistrationTime &&
								<div className='a-is-b'>
									<div>Registration dates:&nbsp;</div>
									<div>
										{ (() => {
											let registrationTimes = monthDayYear(this.props.data.regularRegistrationTime)
											if(this.props.data.endRegistrationTime)
												registrationTimes += `-${ monthDayYear(this.props.data.endRegistrationTime) }`
											return registrationTimes
										})()}
									</div>
								</div>
							}
					
							{ this.props.data.scheduleDays &&
								<div className='a-is-b'>
									<div>Days of the week:&nbsp;</div>
									<div>{ this.props.data.scheduleDays.split(',').join(', ') }</div>
								</div>
							}
					
							{ this.props.data.location &&
								<div className='a-is-b'>
									<div>Location:&nbsp;</div>
									{ (() => {
										if(this.props.data.locationUrlHtml)
											return <a href={ this.props.data.locationUrlHtml } style={ this.props.theme.links ? { color : this.props.theme.links } : { color : '#000000' } } target='_blank' rel='noopener noreferrer'><span>{ this.props.data.location }</span></a>
										else
											return <span>{ this.props.data.location }</span>
									})() }
								</div>
							}
						
						</div>
						
						{ this.props.data.summary &&
							<div className='listing-section'>
								<div className='listing-section-heading'>Summary:&nbsp;</div>
								<div className='description summary'>
										<div>{ this.props.data.summary }</div>
								</div>
							</div>
						}
						
						<div className='listing-section'>
						
							<div>{ (() => {
							
								let description = []
								
								if(this.props.data.gender)
									if(this.props.data.gender === 'CO_ED')
										description.push('Co-Ed')
									else if(this.props.data.gender === 'ANY')
										description.push('Any gender')
									else if(this.props.data.gender === 'MALE')
										description.push('Male')
									else if(this.props.data.gender === 'FEMALE')
										description.push('Female')
								if(this.props.data.experienceLevel)
									description.push(capitalizeFirstLetters(String(this.props.data.experienceLevel)))
								if(this.props.data.season)
									description.push(this.props.data.season)
								if(this.props.data.sport)
									description.push(this.props.data.sport)
								if(this.props.data.type) {
									if(this.props.data.type === 'CLUBTEAM')
										description.push('Club Team')
									else
										description.push(capitalizeFirstLetters(String(this.props.data.type)))
								}
									
								return description.join(' • ')
							
							})() }</div>
								
						</div>
						
						{ ( this.props.data.earlyFreeAgentFee || this.props.data.freeAgentFee || this.props.data.lateFreeAgentFee ) &&
							<div className='listing-section'>
								
								
								
								
								{ ( this.props.data.earlyIndividualFee || this.props.data.teamIndividualFee || this.props.data.lateTeamIndividualFee ) &&
									<div className='la_teamPricing'>
										<div>
											<div>Pricing for free agents:</div>
											<Price
												name='Early'
												cost={ this.props.data.earlyIndividualFee }
												percentProcessingFee={ this.props.data.percentageProcessingFee }
												dollarProcessingFee={ this.props.data.individualProcessingFee }
											/>
											<Price
												name='Regular'
												cost={ this.props.data.teamIndividualFee }
												percentProcessingFee={ this.props.data.percentageProcessingFee }
												dollarProcessingFee={ this.props.data.individualProcessingFee }
											/>
											<Price
												name='Late'
												cost={ this.props.data.lateTeamIndividualFee }
												percentProcessingFee={ this.props.data.percentageProcessingFee }
												dollarProcessingFee={ this.props.data.individualProcessingFee }
											/>
										</div>
									</div>
								}
								
								
								
								
								{ ( this.props.data.earlyTeamFee || this.props.data.teamFee || this.props.data.lateTeamFee ) &&
									<div className='la_teamPricing'>
										<div>
											<div>Pricing for teams:</div>
											<Price
												name='Early'
												cost={ this.props.data.earlyTeamFee }
												percentProcessingFee={ this.props.data.percentageProcessingFee }
												dollarProcessingFee={ this.props.data.teamProcessingFee }
											/>
											<Price
												name='Regular'
												cost={ this.props.data.teamFee }
												percentProcessingFee={ this.props.data.percentageProcessingFee }
												dollarProcessingFee={ this.props.data.teamProcessingFee }
											/>
											<Price
												name='Late'
												cost={ this.props.data.lateTeamFee }
												percentProcessingFee={ this.props.data.percentageProcessingFee }
												dollarProcessingFee={ this.props.data.teamProcessingFee }
											/>
										</div>
									</div>
								}
								
							
								{ this.props.data.hasPaymentPlans &&
									<div className='payment-plan'>(Payment Plan Available)</div>
								}
							
							</div>
						}
					
					</div>
					
					<div className='button'
						style={ (() => {
							let border = this.props.theme.widgetButton ? `1px solid ${ this.props.theme.widgetButton }` : '1px solid #000000'
							let color = this.props.theme.widgetButtonLinkText ? this.props.theme.widgetButtonLinkText : '#000000'
							return { border, color }
						})() }
					>Register</div>
				
				</a>
			</div>
		)
	}
	
}